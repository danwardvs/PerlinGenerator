/*
 * Main
 * This is the main for 12 Num Baseball
 * Calls state machine update and draw functions
 * 2017/11/14
 */
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>


#include "init.h"
#include "state.h"
#include "Noise.h"

// Current state object
state *currentState = nullptr;

// FPS system variables
int fps;
double old_time = 0;
const float MAX_FPS = 60;
int frames_array[100];
int frame_index = 0;

// Closing or naw
bool closing = false;
bool joystick_enabled = false;
bool verbose = false;

// Allegro events
ALLEGRO_EVENT_QUEUE* event_queue = nullptr;
ALLEGRO_TIMER* timer = nullptr;
ALLEGRO_DISPLAY *display = nullptr;


// Delete game state and free state resources
void clean_up(){
  delete currentState;
}

// Change game screen
void change_state(){
  //If the state needs to be changed
  if( nextState != STATE_NULL ){
    //Delete the current state
    if( nextState != STATE_EXIT ){
      delete currentState;
    }

    //Change the state
    switch( nextState ){
      case STATE_INIT:
        currentState = new init();
        break;
      case STATE_NOISE:
        currentState = new Noise();
        break;
      case STATE_EXIT:
        std::cout<<"Exiting program.\n";
        closing = true;
        break;
      default:
        currentState = new Noise();
    }

    //Change the current state ID
    stateID = nextState;

    //NULL the next state ID
    nextState = STATE_NULL;
  }
}

// Setup game
void setup(){

  //srand(time(NULL));

  if(verbose)std::cout<<"Initializing Allegro.";
  // Init allegro
  if( !al_init())
    tools::abort_on_error( "Allegro could not initilize", "Error");

  // Window title
  al_set_window_title(display,"Loading...");

  // Controls
  al_install_keyboard();
  al_install_mouse();
  al_install_joystick();

  // GFX addons
  al_init_image_addon();
  al_init_primitives_addon();

  // Font
  al_init_ttf_addon();

  // Audio
  al_install_audio();
  al_init_acodec_addon();
  al_reserve_samples( 20);

  // Aquire screen

  //al_set_new_display_flags(ALLEGRO_FULLSCREEN);
  display = al_create_display(800, 600);

  if( !display)
    tools::abort_on_error( "Screen could not be created", "Error");

  // Timer
  timer = al_create_timer(1.0 / MAX_FPS);

  // Register events
  event_queue = al_create_event_queue();
  al_register_event_source( event_queue, al_get_display_event_source(display));
  al_register_event_source( event_queue, al_get_timer_event_source(timer));
  al_register_event_source( event_queue, al_get_keyboard_event_source());
  al_register_event_source( event_queue, al_get_joystick_event_source());

  // Timer
  al_start_timer(timer);

  // Window title
  al_set_window_title(display,"Perlin Generation");


  if(verbose){
    std::cout<<" Sucesss.\n";

    // Probably never going to be relevant but pretty cool anyways
    uint32_t version = al_get_allegro_version();
    int major = version >> 24;
    int minor = (version >> 16) & 255;
    int revision = (version >> 8) & 255;
    int release = version & 255;

    std::cout<<"Allegro version "<<major<<"."<<minor<<"."<<revision<<"."<<release<<"\n";

    // This is actually completely irrelevant other than making fun of Allan's PC when he runs this
    // Sorry, your PC is a very nice PC
    std::cout<<"Running as "<<al_get_app_name()<<", with "<<al_get_ram_size()<<" MB RAM.\n";
  }
}

// Handle events
void update(){
  // Event checking
  ALLEGRO_EVENT ev;
  al_wait_for_event( event_queue, &ev);

  // Timer
  if( ev.type == ALLEGRO_EVENT_TIMER){
    // Change state (if needed)
    change_state();

    // Update state
    currentState -> update();
  }
  // Exit
  else if( ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE){
    closing = true;
  }


  // Drawing
  if( al_is_event_queue_empty(event_queue)){
    // Clear buffer
    al_clear_to_color( al_map_rgb(0,0,0));

    // Draw state graphics
    currentState -> draw();

    // Flip (OpenGL)
    al_flip_display();

    // Update fps buffer
    for( int i = 99; i > 0; i--)
      frames_array[i] = frames_array[i - 1];
    frames_array[0] = (1.0/(al_get_time() - old_time));
    old_time = al_get_time();

    int fps_total = 0;
    for( int i = 0; i < 100; i++)
      fps_total += frames_array[i];

    // FPS = average
    fps = fps_total/100;
   // al_set_window_title(display,tools::convertIntToString(fps).c_str());

  }
}

// Start here
int main(int argc, char* argv[]){
  // Basic init

  for (int i = 1; i < argc; i++) {
    if(i==1){
      Noise::width=tools::convertStringToInt(tools::toString(argv[i]));
    }
    else if(i==2)
      Noise::height=tools::convertStringToInt(tools::toString(argv[i]));
    else if(i==2)
      Noise::height=tools::convertStringToInt(tools::toString(argv[i]));
    else if(i==3)
      Noise::inverse= (strcmp(argv[i], "1")==0);
    else if(i==4)
      Noise::background= (strcmp(argv[i], "1")==0);


  }
  std::cout<<"Generation info:\n";

  std::cout<<"Width: ";
  std::cout<<Noise::width;
  std::cout<<"\n";

  std::cout<<"Height: ";
  std::cout<<Noise::height;
  std::cout<<"\n";

  if(Noise::inverse)
    std::cout<<"Drawing inverse\n";
  if(!Noise::background)
    std::cout<<"Drawing transparent background\n";

  setup();

  //Set the current state ID
  stateID = STATE_INIT;
  currentState = new init();

  // Run game
  while(!closing)
    update();

  // Destory display
  al_destroy_display(display);

  return 0;
}
